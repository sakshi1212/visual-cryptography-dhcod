function varargout = reduceresoltool(varargin)
% REDUCERESOLTOOL MATLAB code for reduceresoltool.fig
%      REDUCERESOLTOOL, by itself, creates a new REDUCERESOLTOOL or raises the existing
%      singleton*.
%
%      H = REDUCERESOLTOOL returns the handle to a new REDUCERESOLTOOL or the handle to
%      the existing singleton*.
%
%      REDUCERESOLTOOL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in REDUCERESOLTOOL.M with the given input arguments.
%
%      REDUCERESOLTOOL('Property','Value',...) creates a new REDUCERESOLTOOL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before reduceresoltool_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to reduceresoltool_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help reduceresoltool

% Last Modified by GUIDE v2.5 12-Mar-2014 18:58:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @reduceresoltool_OpeningFcn, ...
                   'gui_OutputFcn',  @reduceresoltool_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before reduceresoltool is made visible.
function reduceresoltool_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to reduceresoltool (see VARARGIN)

% Choose default command line output for reduceresoltool
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes reduceresoltool wait for user response (see UIRESUME)
% uiwait(handles.figure1);
set(gcf,'Name','RRT');
global imagepath;
global image;
global info;
imagepath=getappdata(0,'SharedImage');
image=imread(imagepath);
himage=imshow(imagepath);
impixelinfo(handles.imagepanel,himage);
info=imfinfo(imagepath);
data={info.Width;info.Height;info.FileSize;info.BitDepth;};
set(handles.curproptable,'Data',data);

width1=int2str((3/4)*info.Width);
height1=int2str((3/4)*info.Height);
opt1=strcat(width1,' x ',height1);

width2=int2str((1/2)*info.Width);
height2=int2str((1/2)*info.Height);
opt2=strcat(width2,' x ',height2);

width3=int2str((1/4)*info.Width);
height3=int2str((1/4)*info.Height);
opt3=strcat(width3,' x ',height3);

width4=int2str((1/8)*info.Width);
height4=int2str((1/8)*info.Height);
opt4=strcat(width4,' x ',height4);
options={opt1;opt2;opt3;opt4;};
set(handles.editwidthpop,'String',options);

% --- Outputs from this function are returned to the command line.
function varargout = reduceresoltool_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in editwidthpop.
function editwidthpop_Callback(hObject, eventdata, handles)
% hObject    handle to editwidthpop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns editwidthpop contents as cell array
%        contents{get(hObject,'Value')} returns selected item from editwidthpop


% --- Executes during object creation, after setting all properties.
function editwidthpop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editwidthpop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in previmgbtn.
function previmgbtn_Callback(hObject, eventdata, handles)
% hObject    handle to previmgbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global image;
global w;
global h;
global path;
path='G:\temppreviewimage.jpeg'
J=get(handles.editwidthpop,'Value');
global info;
switch J
    case 1
%         set(handles.text8,'String','one');
        w=(3/4)*info.Width;
        h=(3/4)*info.Height;
        I=imresize(image,[h w]);
        himage=imshow(I); 
        imwrite(I,path,'jpeg');
        
         
    case 2
%         set(handles.text8,'String','two');
         w=(1/2)*info.Width;
        h=(1/2)*info.Height;
         I=imresize(image,[h w]);
        himage=imshow(I);
        imwrite(I,path,'jpeg');
        
    case 3
%         set(handles.text8,'String','three');
        w=(1/4)*info.Width;
        h=(1/4)*info.Height;
         I=imresize(image,[h w]);
        himage=imshow(I);
        imwrite(I,path,'jpeg');
        
      
    case 4
%         set(handles.text8,'String','four');
        w=(1/8)*info.Width;
        h=(1/8)*info.Height;
        I=imresize(image,[h w]);
        himage=imshow(I);
        imwrite(I,path,'jpeg');
        
       
end
 impixelinfo(handles.imagepanel,himage);
 newinfo=imfinfo(path);
 data={newinfo.Width;newinfo.Height;newinfo.FileSize;newinfo.BitDepth;};
 set(handles.newproptable,'Data',data);
 set(handles.text7,'Enable','on');
 set(handles.newproptable,'Enable','on');
set(handles.setbtn,'Enable','on');


        
        
% --- Executes on button press in setbtn.
function setbtn_Callback(hObject, eventdata, handles)
% hObject    handle to setbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global w;
global h;
setappdata(0,'redresolw',w);
setappdata(0,'redresolh',h);
close(gcf);



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
