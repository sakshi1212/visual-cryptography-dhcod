function varargout = dialog1(varargin)
% DIALOG1 MATLAB code for dialog1.fig
%      DIALOG1, by itself, creates a new DIALOG1 or raises the existing
%      singleton*.
%
%      H = DIALOG1 returns the handle to a new DIALOG1 or the handle to
%      the existing singleton*.
%
%      DIALOG1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DIALOG1.M with the given input arguments.
%
%      DIALOG1('Property','Value',...) creates a new DIALOG1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before dialog1_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to dialog1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help dialog1

% Last Modified by GUIDE v2.5 10-Mar-2014 21:06:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @dialog1_OpeningFcn, ...
                   'gui_OutputFcn',  @dialog1_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before dialog1 is made visible.
function dialog1_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to dialog1 (see VARARGIN)

% Choose default command line output for dialog1
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes dialog1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);

set(gcf,'Name','Warning Dialog');
global image;
global himage;
image=getappdata(0,'SelectedImage');
set(handles.pathname,'String',image);
imagedisp=imread(image);
axes(handles.axes2);
himage=imshow(imagedisp,[]);


% --- Outputs from this function are returned to the command line.
function varargout = dialog1_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in selectbtn.
function selectbtn_Callback(hObject, eventdata, handles)
% hObject    handle to selectbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global himage
filename=imsave(himage);
% path=uigetdir('C:\','Select Directory to Save Copy of Secret Image');
set(handles.dirpathname,'String',filename);
%setappdata(0,'NewDir',path);
set(handles.text8,'Enable','on');
set(handles.dirpathname,'Enable','inactive');
set(handles.okbtn,'Enable','on');

function pathname_Callback(hObject, eventdata, handles)
% hObject    handle to pathname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pathname as text
%        str2double(get(hObject,'String')) returns contents of pathname as a double


% --- Executes during object creation, after setting all properties.
function pathname_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pathname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function dirpathname_Callback(hObject, eventdata, handles)
% hObject    handle to dirpathname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dirpathname as text
%        str2double(get(hObject,'String')) returns contents of dirpathname as a double


% --- Executes during object creation, after setting all properties.
function dirpathname_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dirpathname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in okbtn.
function okbtn_Callback(hObject, eventdata, handles)
% hObject    handle to okbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global image;
image=get(handles.dirpathname,'String');
setappdata(0,'SelectedImage',image);
close();
