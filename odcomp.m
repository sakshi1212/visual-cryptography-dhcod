function varargout = odcomp(varargin)
% ODCOMP MATLAB code for odcomp.fig
%      ODCOMP, by itself, creates a new ODCOMP or raises the existing
%      singleton*.
%
%      H = ODCOMP returns the handle to a new ODCOMP or the handle to
%      the existing singleton*.
%
%      ODCOMP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ODCOMP.M with the given input arguments.
%
%      ODCOMP('Property','Value',...) creates a new ODCOMP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before odcomp_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to odcomp_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help odcomp

% Last Modified by GUIDE v2.5 25-Apr-2014 21:54:51

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @odcomp_OpeningFcn, ...
                   'gui_OutputFcn',  @odcomp_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before odcomp is made visible.
function odcomp_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to odcomp (see VARARGIN)

% Choose default command line output for odcomp
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
set(gcf,'Name','Compare between two Ordered Dithering Images');
global secretimage;
global coverimage;
global secret1;
global cover1;
global secret2;
global cover2;
global noise1;
global noise2;
global noiseden1;
global noiseden2;
global val1;
global val2;

global onefirstshare;
global onesecshare;
global twofirstshare;
global twosecshare;
% UIWAIT makes odcomp wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = odcomp_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes when selected object is changed in uipanel6.
function uipanel6_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanel6 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
global secretimage;
global secret1;
global secret2;
selected=get(eventdata.NewValue,'String');
if strcmp(selected,'Small') == 1
      image=imread('G:\SAKSHI-B67-8SEMPROJECT\IMAGES\secretsmall.jpg');
elseif strcmp(selected,'Medium') == 1
   image=imread('G:\SAKSHI-B67-8SEMPROJECT\IMAGES\secretmed.jpg');
else
    image=imread('G:\SAKSHI-B67-8SEMPROJECT\IMAGES\secretlarge.jpg');
end
secretimage=image;
secret1=image;
secret2=image;
axes(handles.axes1);
imshow(image);

% --- Executes when selected object is changed in uipanel5.
function uipanel5_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanel5 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
global coverimage;
global cover1;
global cover2;
selected=get(eventdata.NewValue,'Tag');
if strcmp(selected,'low') == 1
    image=imread('G:\SAKSHI-B67-8SEMPROJECT\IMAGES\sunset.jpg');
else
    image=imread('G:\SAKSHI-B67-8SEMPROJECT\IMAGES\flowers.jpg');
end
coverimage=image;
cover1=image;
cover2=image;
axes(handles.axes2);
imshow(image);

set(handles.text1,'Visible','on');
set(handles.text7,'Visible','on');
set(handles.text3,'Visible','on');
set(handles.text5,'Visible','on');
set(handles.onenoise,'Visible','on');
set(handles.oneden,'Visible','on');
set(handles.onealgo,'Visible','on');

% --- Executes on selection change in onenoise.
function onenoise_Callback(hObject, eventdata, handles)
% hObject    handle to onenoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns onenoise contents as cell array
%        contents{get(hObject,'Value')} returns selected item from onenoise
global noise1;
noise=get(hObject,'Value');
if noise == 1
    noise1='gaussian';
elseif noise == 2
    noise1='poisson'
elseif noise == 3
    noise1 = 'salt & pepper'
else 
    noise1 = 'speckle'
end

set(handles.text9,'Enable','off');
set(handles.text10,'Enable','off');
set(handles.small,'Enable','off');
set(handles.med,'Enable','off');
set(handles.large,'Enable','off');
set(handles.low,'Enable','off');
set(handles.high,'Enable','off');

% --- Executes during object creation, after setting all properties.
function onenoise_CreateFcn(hObject, eventdata, handles)
% hObject    handle to onenoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in oneden.
function oneden_Callback(hObject, eventdata, handles)
% hObject    handle to oneden (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns oneden contents as cell array
%        contents{get(hObject,'Value')} returns selected item from oneden
global noiseden1;
noiseden=get(hObject,'Value');
noiseden1=noiseden/10;


% --- Executes during object creation, after setting all properties.
function oneden_CreateFcn(hObject, eventdata, handles)
% hObject    handle to oneden (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in twonoise.
function twonoise_Callback(hObject, eventdata, handles)
% hObject    handle to twonoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns twonoise contents as cell array
%        contents{get(hObject,'Value')} returns selected item from twonoise
global noise2;
noise=get(hObject,'Value');
if noise == 1
    noise2='gaussian';
elseif noise == 2
    noise2='poisson'
elseif noise == 3
    noise2 = 'salt & pepper'
else 
    noise2 = 'speckle'
end


% --- Executes during object creation, after setting all properties.
function twonoise_CreateFcn(hObject, eventdata, handles)
% hObject    handle to twonoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in twoden.
function twoden_Callback(hObject, eventdata, handles)
% hObject    handle to twoden (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns twoden contents as cell array
%        contents{get(hObject,'Value')} returns selected item from twoden
global noiseden2;
noiseden=get(hObject,'Value');
noiseden2=noiseden/10;


% --- Executes during object creation, after setting all properties.
function twoden_CreateFcn(hObject, eventdata, handles)
% hObject    handle to twoden (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in onealgo.
function onealgo_Callback(hObject, eventdata, handles)
% hObject    handle to onealgo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns onealgo contents as cell array
%        contents{get(hObject,'Value')} returns selected item from onealgo
global val1;
val1=get(hObject,'Value');
set(handles.text2,'Visible','on');
set(handles.text8,'Visible','on');
set(handles.text4,'Visible','on');
set(handles.text6,'Visible','on');
set(handles.twoalgo,'Visible','on');
set(handles.twonoise,'Visible','on');
set(handles.twoden,'Visible','on');

% --- Executes during object creation, after setting all properties.
function onealgo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to onealgo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in twoalgo.
function twoalgo_Callback(hObject, eventdata, handles)
% hObject    handle to twoalgo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns twoalgo contents as cell array
%        contents{get(hObject,'Value')} returns selected item from twoalgo
global val2;
val2=get(hObject,'Value');
set(handles.comparebtn,'Visible','on');

% --- Executes during object creation, after setting all properties.
function twoalgo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to twoalgo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in comparebtn.
function comparebtn_Callback(hObject, eventdata, handles)
% hObject    handle to comparebtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secret1;
global cover1;
global noise1;
global noiseden1;
global val1;
global onefirstshare;
global onesecshare;
global secret2;
global cover2;
global noise2;
global noiseden2;
global val2;
global twofirstshare;
global twosecshare;
global revealone;
global revealtwo;

set(handles.uipanel1,'Title','REVEALED IMAGE FROM SELECTION 1');
set(handles.uipanel2,'Title','REVEALED IMAGE FROM SELECTION 2');

secret1=rgb2gray(secret1);
cover1=rgb2gray(cover1);
secret2=rgb2gray(secret2);
cover2=rgb2gray(cover2);

h=size(secret1,1);
w=size(secret1,2);
cover1=imresize(cover1,[h w]);
cover2=imresize(cover2,[h w]);

secret1=imnoise(secret1,noise1,noiseden1);
secret2=imnoise(secret2, noise2, noiseden2);

secret1=im2bw(secret1);
secret2=im2bw(secret2);

if val1 == 1
    S = [34 29 17 21 30 35;
		 28 14 9 16 20 31;
		 13 8 4 5 15 19;
		 12 3 0 1 10 18; 
		 27 7 2 6 23 24;
		 33 26 11 22 25 32];
elseif val1==2
    S = [34 25 21 17 29 33;
		  30 13 9 5 12 24;
		  18 6 1 0 8 20;
		  22 10 2 3 4 16;
		  26 14 7 11 15 28;
		  35 31 19 23 27 32];
elseif val1==3
    S = [30 22 16 21 33 35;
		  24 11 7 9 26 28;
		  13 5 0 2 14 19;
		  15 3 1 4 12 18;
		  27 8 6 10 25 29;
		  32 20 17 23 31 34];
elseif val1==4
    S1 = [13 9 5 12;
		   6 1 0 8;
		   10 2 3 4;
		   14 7 11 15];
   
	S2 = [18 22 26 19;
		   25 30 31 23;
		   21 29 28 27;
		   17 24 20 16];

	S = [S1 S2; S2 S1];
else
    U = ones(3);
	D = [8 4 5;
		  3 0 1;
		  7 2 6];

	S = [4*D 4*D+2*U;
		  4*D+3*U 4*D+U];
end 
si = size(cover1);
ss = size(S);
ts = ceil(si ./ ss);
SImg = repmat(S, ts);
SImg = SImg(1:si(1), 1:si(2));
SImg = SImg + 0.5;
N = max(S(:)) - min(S(:)) + 2;
D = 255 ./ (N-1);
Q = double(cover1) ./ D;
changedimage = Q > SImg;
cover1=changedimage;

if val2 == 1
    S = [34 29 17 21 30 35;
		 28 14 9 16 20 31;
		 13 8 4 5 15 19;
		 12 3 0 1 10 18; 
		 27 7 2 6 23 24;
		 33 26 11 22 25 32];
elseif val2==2
    S = [34 25 21 17 29 33;
		  30 13 9 5 12 24;
		  18 6 1 0 8 20;
		  22 10 2 3 4 16;
		  26 14 7 11 15 28;
		  35 31 19 23 27 32];
elseif val2==3
    S = [30 22 16 21 33 35;
		  24 11 7 9 26 28;
		  13 5 0 2 14 19;
		  15 3 1 4 12 18;
		  27 8 6 10 25 29;
		  32 20 17 23 31 34];
elseif val2==4
    S1 = [13 9 5 12;
		   6 1 0 8;
		   10 2 3 4;
		   14 7 11 15];
   
	S2 = [18 22 26 19;
		   25 30 31 23;
		   21 29 28 27;
		   17 24 20 16];

	S = [S1 S2; S2 S1];
else
    U = ones(3);
	D = [8 4 5;
		  3 0 1;
		  7 2 6];

	S = [4*D 4*D+2*U;
		  4*D+3*U 4*D+U];
end 
si = size(cover2);
ss = size(S);
ts = ceil(si ./ ss);
SImg = repmat(S, ts);
SImg = SImg(1:si(1), 1:si(2));
SImg = SImg + 0.5;
N = max(S(:)) - min(S(:)) + 2;
D = 255 ./ (N-1);
Q = double(cover2) ./ D;
changedimage = Q > SImg;
cover2=changedimage;

h2=im2bw(secret1);
x1=im2bw(cover1);
sizeh=size(h2,1);
sizew=size(h2,2);
x2=zeros(sizeh,sizew);
for i=1:sizeh
    for j=1:sizew
        h2pixel=h2(i,j);
        if h2pixel == 1 %if pixel in h2 is white
            x2(i,j)= x1(i,j);
        else
            x1pixel=x1(i,j);
            comph2pixel=~h2(i,j);
            x2(i,j)=xor(x1pixel,comph2pixel);
        end
    end
end

h2=im2bw(secret2);
x1=im2bw(cover2);
sizeh=size(h2,1);
sizew=size(h2,2);
x3=zeros(sizeh,sizew);
for i=1:sizeh
    for j=1:sizew
        h2pixel=h2(i,j);
        if h2pixel == 1 %if pixel in h2 is white
            x3(i,j)= x1(i,j);
        else
            x1pixel=x1(i,j);
            comph2pixel=~h2(i,j);
            x3(i,j)=xor(x1pixel,comph2pixel);
        end
    end
end

onefirstshare=cover1;
onesecshare=x2;
twofirstshare=cover2;
twosecshare=x3;
revealone=xor(onefirstshare,onesecshare);
revealtwo=xor(twofirstshare, twosecshare);
axes(handles.axes1);
imshow(revealone);
axes(handles.axes2);
imshow(revealtwo);

set(handles.text1,'Enable','off');
set(handles.text7,'Enable','off');
set(handles.text3,'Enable','off');
set(handles.text5,'Enable','off');
set(handles.onealgo,'Enable','off');
set(handles.onenoise,'Enable','off');
set(handles.oneden,'Enable','off');
set(handles.text2,'Enable','off');
set(handles.text8,'Enable','off');
set(handles.text4,'Enable','off');
set(handles.text6,'Enable','off');
set(handles.twonoise,'Enable','off');
set(handles.twoden,'Enable','off');
set(handles.twoalgo,'Enable','off');
set(handles.backbtn,'Visible','on');

set(handles.onefirstbtn,'Visible','on');
set(handles.onesecbtn,'Visible','on');
set(handles.onerevealbtn,'Visible','on');
set(handles.twofirstbtn,'Visible','on');
set(handles.twosecbtn,'Visible','on');
set(handles.tworevealbtn,'Visible','on');

data1={noise1;noiseden1;'have to calculate';'have to calculate';w;h;'Binary';};
set(handles.table1,'Data',data1);
data2={noise2;noiseden2;'have to calculate';'have to calculate';w;h;'Binary';};
set(handles.table2,'Data',data2);
    

% --- Executes on button press in twofirstbtn.
function twofirstbtn_Callback(hObject, eventdata, handles)
% hObject    handle to twofirstbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global twofirstshare;
setappdata(0,'CompareFullImage',twofirstshare);
fullscreen2();

% --- Executes on button press in twosecbtn.
function twosecbtn_Callback(hObject, eventdata, handles)
% hObject    handle to twosecbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global twosecshare;
setappdata(0,'CompareFullImage',twosecshare);
fullscreen2();

% --- Executes on button press in tworevealbtn.
function tworevealbtn_Callback(hObject, eventdata, handles)
% hObject    handle to tworevealbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global revealtwo;
setappdata(0,'CompareFullImage',revealtwo);
fullscreen2();

% --- Executes on button press in onefirstbtn.
function onefirstbtn_Callback(hObject, eventdata, handles)
% hObject    handle to onefirstbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global onefirstshare;
setappdata(0,'CompareFullImage',onefirstshare);
fullscreen2();

% --- Executes on button press in onesecbtn.
function onesecbtn_Callback(hObject, eventdata, handles)
% hObject    handle to onesecbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global onesecshare;
setappdata(0,'CompareFullImage',onesecshare);
fullscreen2();

% --- Executes on button press in onerevealbtn.
function onerevealbtn_Callback(hObject, eventdata, handles)
% hObject    handle to onerevealbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global revealone;
setappdata(0,'CompareFullImage',revealone);
fullscreen2();


% --- Executes on button press in backbtn.
function backbtn_Callback(hObject, eventdata, handles)
% hObject    handle to backbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
choices();
close(odcomp);
