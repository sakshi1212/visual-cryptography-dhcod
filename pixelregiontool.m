
function varargout = pixelregiontool(varargin)
% PIXELREGIONTOOL MATLAB code for pixelregiontool.fig
%      PIXELREGIONTOOL, by itself, creates a new PIXELREGIONTOOL or raises the existing
%      singleton*.
%
%      H = PIXELREGIONTOOL returns the handle to a new PIXELREGIONTOOL or the handle to
%      the existing singleton*.
%
%      PIXELREGIONTOOL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PIXELREGIONTOOL.M with the given input arguments.
%
%      PIXELREGIONTOOL('Property','Value',...) creates a new PIXELREGIONTOOL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before pixelregiontool_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to pixelregiontool_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help pixelregiontool

% Last Modified by GUIDE v2.5 28-Feb-2014 02:14:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @pixelregiontool_OpeningFcn, ...
                   'gui_OutputFcn',  @pixelregiontool_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before pixelregiontool is made visible.
function pixelregiontool_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to pixelregiontool (see VARARGIN)

% Choose default command line output for pixelregiontool
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

%set(gcf,'Name','PRT');

% UIWAIT makes pixelregiontool wait for user response (see UIRESUME)
% uiwait(handles.figure1);
global pixelimage;
global pixelimagepath;
pixelimagepath=getappdata(0,'SharedImage'); 
pixelimage=imread(pixelimagepath);
axes(handles.axes1);
step=getappdata(0,'Step');
if step == 5
    pixelimagetry=im2bw(pixelimage);
    himage=imshow(pixelimagetry, []);
    imwrite(pixelimagetry,pixelimagepath,'jpeg');
else 
    himage=imshow(pixelimage,[]);
end
hpixinfo=impixelinfo(handles.imagepanel,himage);
%hdrange = imdisplayrange(handles.imagepanel,himage);

hpixreg=impixelregionpanel(hObject,himage);
set(hpixreg,'position',[0.025 0.01 0.95 .34])
% 
% 
% % Display image in the axes and get a handle to the image
% himage = imshow(im);
% 
% % Add Distance tool, specifying axes as parent
% hdist = imdistline(hax);
% 
% % Add Pixel Information tool, specifying image as parent
% hpixinfo = impixelinfo(himage);
% 
% % Add Display Range tool, specifying image as parent
% hdrange = imdisplayrange(himage);
% 
% % Add Pixel Region tool panel, specifying figure as parent
% % and image as target
% hpixreg = impixelregionpanel(hfig,himage);
% 
% % Reposition the Pixel Region tool to fit in the figure
% % window, leaving room for the Pixel Information and
% % Display Range tools.
% set(hpixreg, 'units','normalized','position',[0 .08 1 .4])


% --- Outputs from this function are returned to the command line.
function varargout = pixelregiontool_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
