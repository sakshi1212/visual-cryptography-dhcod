function varargout = fullscreen(varargin)
% FULLSCREEN MATLAB code for fullscreen.fig
%      FULLSCREEN, by itself, creates a new FULLSCREEN or raises the existing
%      singleton*.
%
%      H = FULLSCREEN returns the handle to a new FULLSCREEN or the handle to
%      the existing singleton*.
%
%      FULLSCREEN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FULLSCREEN.M with the given input arguments.
%
%      FULLSCREEN('Property','Value',...) creates a new FULLSCREEN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before fullscreen_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to fullscreen_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help fullscreen

% Last Modified by GUIDE v2.5 16-Apr-2014 20:09:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @fullscreen_OpeningFcn, ...
                   'gui_OutputFcn',  @fullscreen_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before fullscreen is made visible.
function fullscreen_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to fullscreen (see VARARGIN)

% Choose default command line output for fullscreen
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
set(gcf,'Name','Zoom of Image');
imagepath=getappdata(0,'FullImage');
fullimage=imread(imagepath);
axes(handles.axes1);
imshow(fullimage);
% UIWAIT makes fullscreen wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = fullscreen_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
