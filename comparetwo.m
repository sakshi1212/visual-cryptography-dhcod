function varargout = comparetwo(varargin)
% COMPARETWO MATLAB code for comparetwo.fig
%      COMPARETWO, by itself, creates a new COMPARETWO or raises the existing
%      singleton*.
%
%      H = COMPARETWO returns the handle to a new COMPARETWO or the handle to
%      the existing singleton*.
%
%      COMPARETWO('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in COMPARETWO.M with the given input arguments.
%
%      COMPARETWO('Property','Value',...) creates a new COMPARETWO or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before comparetwo_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to comparetwo_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help comparetwo

% Last Modified by GUIDE v2.5 16-Apr-2014 20:05:59

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @comparetwo_OpeningFcn, ...
                   'gui_OutputFcn',  @comparetwo_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before comparetwo is made visible.
function comparetwo_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to comparetwo (see VARARGIN)

% Choose default command line output for comparetwo
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
set(gcf,'Name','View Two Decpyted Images Side by Side');
global odimage;
global edimage;
odimagepath=getappdata(0,'RevealedImagePathOD');
edimagepath=getappdata(0,'RevealedImagePathED');
odimage=imread(odimagepath);
edimage=imread(edimagepath);
axes(handles.axes1);
imshow(odimage);
axes(handles.axes2);
imshow(edimage);
% UIWAIT makes comparetwo wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = comparetwo_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in backbtn.
function backbtn_Callback(hObject, eventdata, handles)
% hObject    handle to backbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
choices();
close(comparetwo);
