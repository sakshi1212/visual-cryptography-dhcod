function varargout = dhcedtool(varargin)
% DHCEDTOOL MATLAB code for dhcedtool.fig
%      DHCEDTOOL, by itself, creates a new DHCEDTOOL or raises the existing
%      singleton*.
%
%      H = DHCEDTOOL returns the handle to a new DHCEDTOOL or the handle to
%      the existing singleton*.
%
%      DHCEDTOOL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DHCEDTOOL.M with the given input arguments.
%
%      DHCEDTOOL('Property','Value',...) creates a new DHCEDTOOL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before dhcedtool_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to dhcedtool_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help dhcedtool

% Last Modified by GUIDE v2.5 16-Apr-2014 20:34:38

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @dhcedtool_OpeningFcn, ...
                   'gui_OutputFcn',  @dhcedtool_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before dhcedtool is made visible.
function dhcedtool_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to dhcedtool (see VARARGIN)

% Choose default command line output for dhcedtool
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
set(gcf,'Name','Encrypt same two image using Error Diffusion Algorithm');
% UIWAIT makes dhcedtool wait for user response (see UIRESUME)
% uiwait(handles.figure1);
global secimage;
global covimage;
secimage=getappdata(0,'SECRETIMAGE');
covimage=getappdata(0,'COVERIMAGE');
setappdata(0,'Step',0);
axes(handles.axes1);
imshow(secimage);
axes(handles.axes2);
imshow(covimage);
setappdata(0,'flag',1);%to depict dhced

% --- Outputs from this function are returned to the command line.
function varargout = dhcedtool_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in viewpropsecbtn.
function viewpropsecbtn_Callback(hObject, eventdata, handles)
% hObject    handle to viewpropsecbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimage;
global secfilename
info=imfinfo(secfilename);
comprat=(((info.Width*info.Height*info.BitDepth)/8)/info.FileSize);
data={info.Filename;info.FileModDate;info.FileSize;info.Format;info.Width;info.Height;info.BitDepth;comprat;info.ColorType;};
set(handles.sectable,'Data',data);

% --- Executes on button press in viewpixsecbtn.
function viewpixsecbtn_Callback(hObject, eventdata, handles)
% hObject    handle to viewpixsecbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimage;
global secfilename;
setappdata(0,'SharedImage',secfilename);
pixelregiontool();

% --- Executes on button press in viewpropcovbtn.
function viewpropcovbtn_Callback(hObject, eventdata, handles)
% hObject    handle to viewpropcovbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global covimage;
global covfilename
info=imfinfo(covfilename);
comprat=(((info.Width*info.Height*info.BitDepth)/8)/info.FileSize);
data={info.Filename;info.FileModDate;info.FileSize;info.Format;info.Width;info.Height;info.BitDepth;comprat;info.ColorType;};
set(handles.covtable,'Data',data);

% --- Executes on button press in viewpixcovbtn.
function viewpixcovbtn_Callback(hObject, eventdata, handles)
% hObject    handle to viewpixcovbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global covimage;
global covfilename;
setappdata(0,'SharedImage',covfilename);
pixelregiontool();

% --- Executes on button press in calcthresbtn.
function calcthresbtn_Callback(hObject, eventdata, handles)
% hObject    handle to calcthresbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimage;
global thresval;
thresval=graythresh(secimage);
set(handles.thresvaltext,'String',thresval);
set(handles.sectobinbtn,'Enable','on');

% --- Executes on button press in sectobinbtn.
function sectobinbtn_Callback(hObject, eventdata, handles)
% hObject    handle to sectobinbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global thresval;
global secimage;
global secfilename;
secimage=im2bw(secimage,thresval);
axes(handles.axes1);
imshow(secimage);
imwrite(secimage,secfilename,'jpeg');
setappdata(0,'Step',5);
set(handles.text10,'Visible','on');
set(handles.covhalftonegenbtn,'Visible','on');

% --- Executes on button press in covhalftonegenbtn.
function covhalftonegenbtn_Callback(hObject, eventdata, handles)
% hObject    handle to covhalftonegenbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global covimage;
global covfilename;
F = [0 0 7;
		 3 5 1];
F = 1/sum(F(:)) .* F;
I = im2double(covimage);
threshold = 0.5;
sf = size(F);
O = zeros(size(I)); %outputimage
% Zero-padding input image, so that we don't have to deal with edges when
% applying the filter. The extra pixels are removed in the output image
I = padarray(I, sf);
padx = sf(2);
pady = sf(1);
si = size(I);
for y = pady+1:si(1)-pady
    for x = padx+1:si(2)-padx
        
        % Threshold image and save to output (taking into account the zero
        % padding introduced)
        oy = y - pady;
        ox = x - padx;
        O(oy,ox) = (I(y,x) > threshold);
        
        % Calculate error
        error = double(I(y,x) - O(oy,ox));
        
        % Get position where the filter should be applied
        k = floor(sf(2)/2);
        ymin = y;
        ymax = y + sf(1) - 1;
        xmin = x - (sf(2) - k - 1);
        xmax = x + k;
        
        % Distribute error according to the filter
        I(ymin:ymax, xmin:xmax) = I(ymin:ymax, xmin:xmax) + error * F;
    end
end
covimage=O;
axes(handles.axes2);
imshow(covimage);
set(handles.text3,'Visible','on');
imwrite(covimage,covfilename,'jpeg');
set(handles.text11,'Visible','on');
set(handles.gensecsharebtn,'Visible','on');

% --- Executes on button press in gensecsharebtn.
function gensecsharebtn_Callback(hObject, eventdata, handles)
% hObject    handle to gensecsharebtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimage;
global covimage;
global secfilename;
sizeh=size(secimage,1);
sizew=size(secimage,2);
x2=zeros(sizeh,sizew);
for i=1:sizeh
    for j=1:sizew
        h2pixel=secimage(i,j);
        if h2pixel == 1 %if pixel in h2 is white
            x2(i,j)= covimage(i,j);
        else
            x1pixel=covimage(i,j);
            comph2pixel=~secimage(i,j);
            x2(i,j)=xor(x1pixel,comph2pixel);
        end
    end
end
%resultingimage=image(matriximage);
axes(handles.axes1);
secimage=x2;
imshow(secimage);
imwrite(secimage,secfilename,'jpeg');
set(handles.nextbtn,'Visible','on');


% --- Executes on button press in savecovbtn.
function savecovbtn_Callback(hObject, eventdata, handles)
% hObject    handle to savecovbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global covfilename;
global covimage;
axes(handles.axes2);
himage=imshow(covimage);
covfilename=imsave(himage);
set(handles.viewpropcovbtn,'Enable','on');
set(handles.viewpixcovbtn,'Enable','on');
set(handles.covtable,'Enable','on');
set(handles.text13,'Visible','on');
set(handles.text14,'Visible','on');
set(handles.text6,'Visible','on');
set(handles.text7,'Visible','on');
set(handles.text8,'Visible','on');
set(handles.calcthresbtn,'Visible','on');
set(handles.thresvaltext,'Visible','on');
set(handles.sectobinbtn,'Visible','on');
set(handles.sectobinbtn,'Enable','off');
set(handles.zoomcover,'Enable','on');

% --- Executes on button press in savesecbtn.
function savesecbtn_Callback(hObject, eventdata, handles)
% hObject    handle to savesecbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secfilename;
global secimage;
axes(handles.axes1);
himage=imshow(secimage);
secfilename=imsave(himage);
set(handles.viewpropsecbtn,'Enable','on');
set(handles.viewpixsecbtn,'Enable','on');
set(handles.sectable,'Enable','on');
set(handles.savecovbtn,'Enable','on');
set(handles.zoomsecret,'Enable','on');

% --- Executes on button press in nextbtn.
function nextbtn_Callback(hObject, eventdata, handles)
% hObject    handle to nextbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimage;
global covimage;
global secfilename;
global covfilename;
imwrite(secimage,secfilename,'jpeg');
imwrite(covimage,covfilename,'jpeg');
setappdata(0,'firstpath',secfilename);
setappdata(0,'secpath',covfilename);
setappdata(0,'firstshare',secimage);
setappdata(0,'secondshare',covimage);
decryptmain();
close(dhcedtool);


% --- Executes on button press in zoomcover.
function zoomcover_Callback(hObject, eventdata, handles)
% hObject    handle to zoomcover (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global covfilename;
setappdata(0,'FullImage',covfilename);
fullscreen();

% --- Executes on button press in zoomsecret.
function zoomsecret_Callback(hObject, eventdata, handles)
% hObject    handle to zoomsecret (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secfilename;
setappdata(0,'FullImage',secfilename);
fullscreen();
