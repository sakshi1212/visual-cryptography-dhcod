function varargout = improve(varargin)
% IMPROVE MATLAB code for improve.fig
%      IMPROVE, by itself, creates a new IMPROVE or raises the existing
%      singleton*.
%
%      H = IMPROVE returns the handle to a new IMPROVE or the handle to
%      the existing singleton*.
%
%      IMPROVE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IMPROVE.M with the given input arguments.
%
%      IMPROVE('Property','Value',...) creates a new IMPROVE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before improve_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to improve_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help improve

% Last Modified by GUIDE v2.5 02-May-2014 13:07:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @improve_OpeningFcn, ...
                   'gui_OutputFcn',  @improve_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before improve is made visible.
function improve_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to improve (see VARARGIN)

% Choose default command line output for improve
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
set(gcf,'Name','Improvement to Given Algorithm');
global cover;
global secret;
cover=getappdata(0,'improvecover');
secret=getappdata(0,'improvesecret');
secret=imcomplement(secret);
secret=bwmorph(secret,'remove');
% axes(handles.axes1);
% imshow(secret);
% UIWAIT makes improve wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = improve_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in backbtn.
function backbtn_Callback(hObject, eventdata, handles)
% hObject    handle to backbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
choices();
close(improve);

% --- Executes on button press in viewfirstsharebtn.
function viewfirstsharebtn_Callback(hObject, eventdata, handles)
% hObject    handle to viewfirstsharebtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global cover;
set(handles.axes1,'Visible','on');
axes(handles.axes1);
imshow(cover);

% --- Executes on button press in viewsecondodbtn.
function viewsecondodbtn_Callback(hObject, eventdata, handles)
% hObject    handle to viewsecondodbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
secondshare=getappdata(0,'impsecondshare');
axes(handles.axes1);
imshow(secondshare);

% --- Executes on button press in viewrevealimage.
function viewrevealimage_Callback(hObject, eventdata, handles)
% hObject    handle to viewrevealimage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
reveal=getappdata(0,'imprevealimage');
axes(handles.axes1);
imshow(reveal);

% --- Executes on button press in secsharimpbtn.
function secsharimpbtn_Callback(hObject, eventdata, handles)
% hObject    handle to secsharimpbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global cover;
global secret;
h2=secret;
x1=cover;
sizeh=size(h2,1);
sizew=size(h2,2);

x2=zeros(sizeh,sizew);

for i=1:sizeh
    for j=1:sizew
        h2pixel=h2(i,j);
        if h2pixel == 1 %if pixel in h2 is white
            x2(i,j)= x1(i,j);
        else
            x1pixel=x1(i,j);
            comph2pixel=~h2(i,j);
            x2(i,j)=xor(x1pixel,comph2pixel);
        end
    end
end
axes(handles.axes1);
imshow(x2);
cover= x1;
secret=x2;


% --- Executes on button press in revealimpbtn.
function revealimpbtn_Callback(hObject, eventdata, handles)
% hObject    handle to revealimpbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global cover;
global secret;
global impreveal;
impreveal=xor(cover, secret);
impreveal=imcomplement(impreveal);
axes(handles.axes1);
imshow(impreveal);


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in fullscreenbtn.
function fullscreenbtn_Callback(hObject, eventdata, handles)
% hObject    handle to fullscreenbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global impreveal;
setappdata(0,'CompareFullImage',impreveal);
fullscreen2;
