function varargout = encryptmain(varargin)
% ENCRYPTMAIN MATLAB code for encryptmain.fig
%      ENCRYPTMAIN, by itself, creates a new ENCRYPTMAIN or raises the existing
%      singleton*.
%
%      H = ENCRYPTMAIN returns the handle to a new ENCRYPTMAIN or the handle to
%      the existing singleton*.
%
%      ENCRYPTMAIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ENCRYPTMAIN.M with the given input arguments.
%
%      ENCRYPTMAIN('Property','Value',...) creates a new ENCRYPTMAIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before encryptmain_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to encryptmain_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help encryptmain

% Last Modified by GUIDE v2.5 16-Apr-2014 20:17:59

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @encryptmain_OpeningFcn, ...
                   'gui_OutputFcn',  @encryptmain_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before encryptmain is made visible.
function encryptmain_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to encryptmain (see VARARGIN)

% Choose default command line output for encryptmain
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
set(gcf,'Name','Encrypt two images using DHCOD'); 
global secimage;
global secimagepath;
global covimage;
global covimagepath;
setappdata(0,'Step',0);
setappdata(0,'flag',0);%flag to show its dhcod
% UIWAIT makes encryptmain wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = encryptmain_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in uploadsecbtn.
function uploadsecbtn_Callback(hObject, eventdata, handles)
% hObject    handle to uploadsecbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimage;
global secimagepath;
filename=imgetfile;
setappdata(0,'SelectedImage',filename);
dialog1();
uiwait(gcf);
secimagepath=getappdata(0,'SelectedImage');
secimage=imread(secimagepath);
axes(handles.axes1);
imshow(secimage);
setappdata(0,'SECRET',secimage);
setappdata(0,'SECRETPATH',secimagepath);
set(handles.text1,'Visible','on');
set(handles.secpropbtn,'Visible','on');
set(handles.secpixbtn,'Visible','on');
set(handles.secproptable,'Visible','on');
set(handles.uploadcovbtn,'Enable','on');
set(handles.fullsecret,'Enable','on');
set(handles.uploadsecbtn,'Enable','off');

% --- Executes on button press in uploadcovbtn.
function uploadcovbtn_Callback(hObject, eventdata, handles)
% hObject    handle to uploadcovbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global covimage;
global covimagepath;
filename=imgetfile;
setappdata(0,'SelectedImage',filename);
dialog1();
uiwait(gcf);
covimagepath=getappdata(0,'SelectedImage');
covimage=imread(covimagepath);
axes(handles.axes2);
imshow(covimage);
setappdata(0,'COVER',covimage);
setappdata(0,'COVERPATH',covimagepath);
set(handles.text4,'Visible','on');
set(handles.covpropbtn,'Visible','on');
set(handles.covpixbtn,'Visible','on');
set(handles.covproptable,'Visible','on');
set(handles.uipanel3,'Visible','on');
set(handles.text9,'Visible','on');
set(handles.sec2graybtn,'Visible','on');
set(handles.cov2graybtn,'Visible','on');
set(handles.uploadcovbtn,'Enable','off');
set(handles.fullcover,'Enable','on');

% --- Executes on button press in sec2graybtn.
function sec2graybtn_Callback(hObject, eventdata, handles)
% hObject    handle to sec2graybtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimage;
global secimagepath;
secimage=rgb2gray(secimage);
imwrite(secimage,secimagepath,'jpeg');
setappdata(0,'SECRET',secimage);
axes(handles.axes1);
imshow(secimage);
set(handles.cov2graybtn,'Enable','on');
set(handles.sec2graybtn,'Enable','off');
set(handles.text1,'String','Image converted to Grayscale');

% --- Executes on button press in secpropbtn.
function secpropbtn_Callback(hObject, eventdata, handles)
% hObject    handle to secpropbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimagepath;
info=imfinfo(secimagepath);
comprat=(((info.Width*info.Height*info.BitDepth)/8)/info.FileSize);
data={info.Filename;info.FileModDate;info.FileSize;info.Format;info.Width;info.Height;info.BitDepth;comprat;info.ColorType;};
set(handles.secproptable,'Data',data);

% --- Executes on button press in secpixbtn.
function secpixbtn_Callback(hObject, eventdata, handles)
% hObject    handle to secpixbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimagepath;
setappdata(0,'SharedImage',secimagepath);
pixelregiontool();

% --- Executes on button press in covpropbtn.
function covpropbtn_Callback(hObject, eventdata, handles)
% hObject    handle to covpropbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global covimagepath;
info=imfinfo(covimagepath);
comprat=(((info.Width*info.Height*info.BitDepth)/8)/info.FileSize);
data={info.Filename;info.FileModDate;info.FileSize;info.Format;info.Width;info.Height;info.BitDepth;comprat;info.ColorType;};
set(handles.covproptable,'Data',data);

% --- Executes on button press in covpixbtn.
function covpixbtn_Callback(hObject, eventdata, handles)
% hObject    handle to covpixbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global covimagepath;
setappdata(0,'SharedImage',covimagepath);
pixelregiontool();

% --- Executes on button press in cov2graybtn.
function cov2graybtn_Callback(hObject, eventdata, handles)
% hObject    handle to cov2graybtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global covimage;
global covimagepath;
covimage=rgb2gray(covimage);
imwrite(covimage,covimagepath,'jpeg');
setappdata(0,'COVER',covimage);
axes(handles.axes2);
imshow(covimage);
set(handles.text10,'Visible','on');
set(handles.secresolbtn,'Visible','on');
set(handles.covscalebtn,'Visible','on');
set(handles.cov2graybtn,'Enable','off');
set(handles.text4,'String','Image converted to Grayscale');

% --- Executes on button press in secresolbtn.
function secresolbtn_Callback(hObject, eventdata, handles)
% hObject    handle to secresolbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimagepath;
global secimage;
global w;
global h;
setappdata(0,'SharedImage',secimagepath);
reduceresoltool();
uiwait(gcf);
w=getappdata(0,'redresolw');
h=getappdata(0,'redresolh');
secimage=imresize(secimage,[h w]);
imwrite(secimage,secimagepath,'jpeg');
setappdata(0,'SECRET',secimage);
setappdata(0,'SECRETIMAGE',secimage);%FOR USE ON DHCED SCREEN
axes(handles.axes1);
imshow(secimage);
set(handles.covscalebtn,'Enable','on');
set(handles.secresolbtn,'Enable','off');
set(handles.text1,'String','Image Resized');


% --- Executes on button press in covscalebtn.
function covscalebtn_Callback(hObject, eventdata, handles)
% hObject    handle to covscalebtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global covimage;
global covimagepath;
global w;
global h;
covimage=imresize(covimage,[h w]);
imwrite(covimage,covimagepath,'jpeg');
setappdata(0,'COVER',covimage);
setappdata(0,'COVERIMAGE',covimage);%FOR USE ON DHCED SCREEN
axes(handles.axes2);
imshow(covimage);
set(handles.text13,'Visible','on');
set(handles.text15,'Visible','on');
set(handles.noisetype,'Visible','on');
set(handles.text16,'Visible','on');
set(handles.noisedenslider,'Visible','on');
set(handles.noisedenval,'Visible','on');
set(handles.previewnoisebtn,'Visible','on');
set(handles.covscalebtn,'Enable','off');
set(handles.text4,'String','Image Scaled');


% --- Executes on selection change in noisetype.
function noisetype_Callback(hObject, eventdata, handles)
% hObject    handle to noisetype (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns noisetype contents as cell array
%        contents{get(hObject,'Value')} returns selected item from noisetype
set(handles.text16,'Enable','on');
set(handles.noisedenslider,'Enable','on');
set(handles.noisedenval,'Enable','on');



% --- Executes during object creation, after setting all properties.
function noisetype_CreateFcn(hObject, eventdata, handles)
% hObject    handle to noisetype (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function noisedenslider_Callback(hObject, eventdata, handles)
% hObject    handle to noisedenslider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global density;
density=get(hObject,'Value');
set(handles.noisedenval,'String',density);
set(handles.previewnoisebtn,'Enable','on');

% --- Executes during object creation, after setting all properties.
function noisedenslider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to noisedenslider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in previewnoisebtn.
function previewnoisebtn_Callback(hObject, eventdata, handles)
% hObject    handle to previewnoisebtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimagepath;
global secimage;
global density;
noise=get(handles.noisetype,'Value');
setappdata(0,'NoiseType',noise);
setappdata(0,'NoiseDen',density);
setappdata(0,'SharedImage',secimagepath);
previewnoisetool();
uiwait()
secimage=imread(secimagepath);
axes(handles.axes1);
imshow(secimage);
setappdata(0,'SECRET',secimage);
set(handles.text12,'Visible','on');
set(handles.calcthresbtn,'Visible','on');
set(handles.thresvaltxt,'Visible','on');
set(handles.sec2binbtn,'Visible','on');
set(handles.text15,'Enable','off');
set(handles.noisetype,'Enable','off');
set(handles.text16,'Enable','off');
set(handles.noisedenslider,'Enable','off');
set(handles.noisedenval,'Enable','off');
set(handles.previewnoisebtn,'Enable','off');
set(handles.text1,'String','Noise Added To Image');

% --- Executes on button press in calcthresbtn.
function calcthresbtn_Callback(hObject, eventdata, handles)
% hObject    handle to calcthresbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimage;
global threshold;
threshold=graythresh(secimage);
set(handles.thresvaltxt,'String',threshold);
set(handles.sec2binbtn,'Enable','on');

% --- Executes on button press in sec2binbtn.
function sec2binbtn_Callback(hObject, eventdata, handles)
% hObject    handle to sec2binbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimagepath;
global secimage;
global threshold;
setappdata(0,'Step',5);
secimagetry=im2bw(secimage,threshold);
axes(handles.axes1);
imshow(secimagetry);
imwrite(secimagetry,secimagepath,'jpeg');
setappdata(0,'SECRET',secimagetry);
set(handles.text11,'Visible','on');
set(handles.cov2halftonebtn,'Visible','on');
set(handles.calcthresbtn,'Enable','off');
set(handles.thresvaltxt,'Enable','off');
set(handles.sec2binbtn,'Enable','off');
set(handles.covpixbtn,'Enable','off');
set(handles.text1,'String','Image Converted to Binary');
setappdata(0,'improvesecret',secimagetry);

% --- Executes on button press in cov2halftonebtn.
function cov2halftonebtn_Callback(hObject, eventdata, handles)
% hObject    handle to cov2halftonebtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global covimagepath;
global covimage;
setappdata(0,'SharedImage',covimagepath);
halftonebydithering();
uiwait();
covimage=imread(covimagepath);
axes(handles.axes2);
imshow(covimage);
setappdata(0,'improvecover',covimage);
setappdata(0,'firstshare',covimage);
setappdata(0,'COVER',covimage);
set(handles.text7,'Visible','on');
set(handles.gen2ndsharebtn,'Visible','on');
set(handles.nextbtn,'Visible','on');
set(handles.text4,'String','Halftone Image Generated by Ordered Dithering');
set(handles.text3,'Visible','on');
set(handles.covpixbtn,'Enable','on');
set(handles.cov2halftonebtn,'Enable','off');

% --- Executes on button press in gen2ndsharebtn.
function gen2ndsharebtn_Callback(hObject, eventdata, handles)
% hObject    handle to gen2ndsharebtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimagepath;
global secimage;
global covimagepath;
global covimage;

secimage=imread(secimagepath);
h2=im2bw(secimage);

covimage=imread(covimagepath);
x1=im2bw(covimage);

sizeh=size(h2,1);
sizew=size(h2,2);

x2=zeros(sizeh,sizew);

for i=1:sizeh
    for j=1:sizew
        h2pixel=h2(i,j);
        if h2pixel == 1 %if pixel in h2 is white
            x2(i,j)= x1(i,j);
        else
            x1pixel=x1(i,j);
            comph2pixel=~h2(i,j);
            x2(i,j)=xor(x1pixel,comph2pixel);
        end
    end
end

axes(handles.axes1);
imshow(x2);
imwrite(x2,secimagepath,'jpeg');
setappdata(0,'secondshare',x2);
setappdata(0,'impsecondshare',x2);
set(handles.nextbtn,'Enable','on');
set(handles.text1,'String','Second Share Generated');
set(handles.text2,'Visible','on');
set(handles.gen2ndsharebtn,'Enable','off');

% --- Executes on button press in nextbtn.
function nextbtn_Callback(hObject, eventdata, handles)
% hObject    handle to nextbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimagepath;
global covimagepath;
setappdata(0,'firstpath',covimagepath);
setappdata(0,'secpath',secimagepath);
decryptmain();
close(encryptmain);


% --- Executes on button press in fullsecret.
function fullsecret_Callback(hObject, eventdata, handles)
% hObject    handle to fullsecret (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimagepath;
setappdata(0,'FullImage',secimagepath);
fullscreen();

% --- Executes on button press in fullcover.
function fullcover_Callback(hObject, eventdata, handles)
% hObject    handle to fullcover (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global covimagepath;
setappdata(0,'FullImage',covimagepath);
fullscreen();
