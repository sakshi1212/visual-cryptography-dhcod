function varargout = decryptmain(varargin)
% DECRYPTMAIN MATLAB code for decryptmain.fig
%      DECRYPTMAIN, by itself, creates a new DECRYPTMAIN or raises the existing
%      singleton*.
%
%      H = DECRYPTMAIN returns the handle to a new DECRYPTMAIN or the handle to
%      the existing singleton*.
%
%      DECRYPTMAIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DECRYPTMAIN.M with the given input arguments.
%
%      DECRYPTMAIN('Property','Value',...) creates a new DECRYPTMAIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before decryptmain_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to decryptmain_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help decryptmain

% Last Modified by GUIDE v2.5 16-Apr-2014 20:33:39

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @decryptmain_OpeningFcn, ...
                   'gui_OutputFcn',  @decryptmain_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before decryptmain is made visible.
function decryptmain_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to decryptmain (see VARARGIN)

% Choose default command line output for decryptmain
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
set(gcf,'Name','Decrypt Secret Image');
global first;
global second;
global firstpath;
global secpath;
global revealedimage;
firstpath=getappdata(0,'firstpath');
secpath=getappdata(0,'secpath');
first1=getappdata(0,'firstshare');
second2=getappdata(0,'secondshare');
first=im2bw(first1);
second=im2bw(second2);
axes(handles.axes1);
imshow(first);
axes(handles.axes2);
imshow(second);
%display properties of first image
infofirst=imfinfo(firstpath);
comprat=(((infofirst.Width*infofirst.Height*infofirst.BitDepth)/8)/infofirst.FileSize);
datafirst={infofirst.Filename;infofirst.FileModDate;infofirst.FileSize;infofirst.Format;infofirst.Width;infofirst.Height;infofirst.BitDepth;comprat;'Grayscale-Binary';};
set(handles.firsttable,'Data',datafirst);
%display properties of second image
infosec=imfinfo(secpath);
comprat=(((infosec.Width*infosec.Height*infosec.BitDepth)/8)/infosec.FileSize);
datasec={infosec.Filename;infosec.FileModDate;infosec.FileSize;infosec.Format;infosec.Width;infosec.Height;infosec.BitDepth;comprat;'Grayscale-Binary';};
set(handles.secondtable,'Data',datasec);

% UIWAIT makes decryptmain wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = decryptmain_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in revealxor.
function revealxor_Callback(hObject, eventdata, handles)
% hObject    handle to revealxor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global first;
global second;
global revealedimage;
revealedimage=xor(second,first);
axes(handles.axes3);
imshow(revealedimage);
set(handles.revealedtext,'Visible','on');
set(handles.revealtable,'Visible','on');
set(handles.saveimagebtn,'Visible','on');
set(handles.viewpixelsrevealed,'Visible','on');
set(handles.nextbtn,'Visible','on');
fn='File not saved.'
w=size(revealedimage,2);
h=size(revealedimage,1);
col='Binary';
data={fn;w;h;col};
set(handles.revealtable,'Data',data);

% --- Executes on button press in revealand.
function revealand_Callback(hObject, eventdata, handles)
% hObject    handle to revealand (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global first;
global second;
global revealedimage;
revealedimage=and(second,first);
axes(handles.axes3);
imshow(revealedimage);
set(handles.revealedtext,'Visible','on');
set(handles.revealtable,'Visible','on');
set(handles.saveimagebtn,'Visible','on');
set(handles.viewpixelsrevealed,'Visible','on');
set(handles.nextbtn,'Visible','on');
fn='File not saved.'
w=size(revealedimage,2);
h=size(revealedimage,1);
col='Binary';
data={fn;w;h;col};
set(handles.revealtable,'Data',data);

% --- Executes on button press in revealor.
function revealor_Callback(hObject, eventdata, handles)
% hObject    handle to revealor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global first;
global second;
global revealedimage;
revealedimage=or(second,first);
axes(handles.axes3);
imshow(revealedimage);
set(handles.revealedtext,'Visible','on');
set(handles.revealtable,'Visible','on');
set(handles.saveimagebtn,'Visible','on');
set(handles.viewpixelsrevealed,'Visible','on');
set(handles.nextbtn,'Visible','on');
fn='File not saved.'
w=size(revealedimage,2);
h=size(revealedimage,1);
col='Binary';
data={fn;w;h;col};
set(handles.revealtable,'Data',data);

% --- Executes on button press in viewpixelsfirst.
function viewpixelsfirst_Callback(hObject, eventdata, handles)
% hObject    handle to viewpixelsfirst (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global firstpath;
setappdata(0,'Step',5);
setappdata(0,'SharedImage',firstpath);
pixelregiontool();

% --- Executes on button press in viewpixelssecond.
function viewpixelssecond_Callback(hObject, eventdata, handles)
% hObject    handle to viewpixelssecond (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secpath;
setappdata(0,'SharedImage',secpath);
setappdata(0,'Step',5);
pixelregiontool();

% --- Executes on button press in saveimagebtn.
function saveimagebtn_Callback(hObject, eventdata, handles)
% hObject    handle to saveimagebtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global revealedimage;
global filename;
axes(handles.axes3);
setappdata(0,'imprevealimage',revealedimage);
himage=imshow(revealedimage);
filename=imsave(himage);
w=size(revealedimage,2);
h=size(revealedimage,1);
data={filename;w;h;'Binary'};
set(handles.revealtable,'Data',data);
set(handles.saveimagetext,'Visible','on');
set(handles.nextbtn,'Enable','on');
set(handles.zoomreveal,'Enable','on');
set(handles.viewpixelsrevealed,'Enable','on');
flag=getappdata(0,'flag');
if flag == 0 %if dhcod
    setappdata(0,'RevealedImagePathOD',filename);
else
    setappdata(0,'RevealedImagePathED',filename);   
end


% --- Executes on button press in viewpixelsrevealed.
function viewpixelsrevealed_Callback(hObject, eventdata, handles)
% hObject    handle to viewpixelsrevealed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global filename;
setappdata(0,'Step',5);
setappdata(0,'SharedImage',filename);
pixelregiontool();

% --- Executes on button press in nextbtn.
function nextbtn_Callback(hObject, eventdata, handles)
% hObject    handle to nextbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
flag=getappdata(0,'flag');
if flag == 0 %if dhcod
    choices();
else
    comparetwo(); 
end
close(decryptmain);


% --- Executes on button press in fullsecond.
function fullsecond_Callback(hObject, eventdata, handles)
% hObject    handle to fullsecond (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secpath;
setappdata(0,'FullImage',secpath);
fullscreen();

% --- Executes on button press in fullfirst.
function fullfirst_Callback(hObject, eventdata, handles)
% hObject    handle to fullfirst (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global firstpath;
setappdata(0,'FullImage',firstpath);
fullscreen();


% --- Executes on button press in zoomreveal.
function zoomreveal_Callback(hObject, eventdata, handles)
% hObject    handle to zoomreveal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global filename;
setappdata(0,'FullImage',filename);
fullscreen();