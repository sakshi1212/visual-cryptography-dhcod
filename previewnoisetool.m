function varargout = previewnoisetool(varargin)
% PREVIEWNOISETOOL MATLAB code for previewnoisetool.fig
%      PREVIEWNOISETOOL, by itself, creates a new PREVIEWNOISETOOL or raises the existing
%      singleton*.
%
%      H = PREVIEWNOISETOOL returns the handle to a new PREVIEWNOISETOOL or the handle to
%      the existing singleton*.
%
%      PREVIEWNOISETOOL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PREVIEWNOISETOOL.M with the given input arguments.
%
%      PREVIEWNOISETOOL('Property','Value',...) creates a new PREVIEWNOISETOOL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before previewnoisetool_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to previewnoisetool_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help previewnoisetool

% Last Modified by GUIDE v2.5 30-Apr-2014 21:43:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @previewnoisetool_OpeningFcn, ...
                   'gui_OutputFcn',  @previewnoisetool_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before previewnoisetool is made visible.
function previewnoisetool_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to previewnoisetool (see VARARGIN)

% Choose default command line output for previewnoisetool
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
set(gcf,'Name','PNT');
global noiseimage;
global noiseimagepath;
global noisetype;
global noiseden;
noiseimagepath = getappdata(0,'SharedImage'); 
noiseimage=imread(noiseimagepath);
axes(handles.axes1);
himage=imshow(noiseimage, []);
hpixinfo=impixelinfo(handles.imagepanel,himage);
noisetype=getappdata(0,'NoiseType');
noiseden=getappdata(0,'NoiseDen');
set(handles.noisedentxtbox,'String',noiseden);
switch noisetype
    case 1
        set(handles.noisetypetxtbox,'String','Gaussian Noise')
         set(handles.noisedentxtbox,'Enable','on');
         set(handles.noisedenslider,'Enable','on');
         set(handles.noisedenval,'Enable','on');
         set(handles.text6,'Enable','on');
          set(handles.text7,'Enable','on');
        J=imnoise(noiseimage,'gaussian',noiseden);
        axes(handles.axes1);
        imshow(J,[]);
       
    case 2
         set(handles.noisetypetxtbox,'String','Poisson Noise')
         %set(handles.noisetypelb,2);
         set(handles.noisedentxtbox,'Enable','off');
         set(handles.noisedenslider,'Enable','off');
         set(handles.noisedenval,'Enable','off');
          set(handles.text6,'Enable','off');
          set(handles.text7,'Enable','off');
         J=imnoise(noiseimage,'poisson');
         axes(handles.axes1);
        imshow(J,[]);
        
    case 3
         set(handles.noisetypetxtbox,'String','Salt and Pepper Noise');
          set(handles.noisedentxtbox,'Enable','on');
         set(handles.noisedenslider,'Enable','on');
         set(handles.noisedenval,'Enable','on');
          set(handles.text6,'Enable','on');
          set(handles.text7,'Enable','on');
         %set(handles.noisetypelb,3);
         J=imnoise(noiseimage,'salt & pepper',noiseden);
         axes(handles.axes1);
        imshow(J,[]);
        
    case 4
         set(handles.noisetypetxtbox,'String','Speckle Noise');
          set(handles.noisedentxtbox,'Enable','on');
         set(handles.noisedenslider,'Enable','on');
         set(handles.noisedenval,'Enable','on');
          set(handles.text6,'Enable','on');
          set(handles.text7,'Enable','on');
         J=imnoise(noiseimage,'speckle',noiseden);
         %set(handles.noisetypelb,4);
         axes(handles.axes1);
        imshow(J,[]);
        
    otherwise 
        set(handles.noisetypetxtbox,'String','stupid')
end


% --- Outputs from this function are returned to the command line.
function varargout = previewnoisetool_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function noisetypetxtbox_Callback(hObject, eventdata, handles)
% hObject    handle to noisetypetxtbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of noisetypetxtbox as text
%        str2double(get(hObject,'String')) returns contents of noisetypetxtbox as a double


% --- Executes during object creation, after setting all properties.
function noisetypetxtbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to noisetypetxtbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject,'Enable','inactive');


function noisedentxtbox_Callback(hObject, eventdata, handles)
% hObject    handle to noisedentxtbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of noisedentxtbox as text
%        str2double(get(hObject,'String')) returns contents of noisedentxtbox as a double


% --- Executes during object creation, after setting all properties.
function noisedentxtbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to noisedentxtbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject,'Enable','inactive');


% --- Executes on selection change in noisetypelb.
function noisetypelb_Callback(hObject, eventdata, handles)
% hObject    handle to noisetypelb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns noisetypelb contents as cell array
%        contents{get(hObject,'Value')} returns selected item from noisetypelb
i=get(hObject,'Value');
switch i
    case 1
        set(handles.noisedentxtbox,'Enable','on');
         set(handles.noisedenslider,'Enable','on');
         set(handles.noisedenval,'Enable','on');
         set(handles.text6,'Enable','on');
          set(handles.text7,'Enable','on');
    case 2
        set(handles.noisedentxtbox,'Enable','off');
         set(handles.noisedenslider,'Enable','off');
         set(handles.noisedenval,'Enable','off');
         set(handles.text6,'Enable','off');
          set(handles.text7,'Enable','off');
    case 3
        set(handles.noisedentxtbox,'Enable','on');
         set(handles.noisedenslider,'Enable','on');
         set(handles.noisedenval,'Enable','on');
         set(handles.text6,'Enable','on');
          set(handles.text7,'Enable','on');
    case 4
        set(handles.noisedentxtbox,'Enable','on');
         set(handles.noisedenslider,'Enable','on');
         set(handles.noisedenval,'Enable','on');
         set(handles.text6,'Enable','on');
          set(handles.text7,'Enable','on');
end
% --- Executes during object creation, after setting all properties.
function noisetypelb_CreateFcn(hObject, eventdata, handles)
% hObject    handle to noisetypelb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
data={'Gaussian noise';'Poisson Noise';'Salt and Pepper noise';'Speckle';};
set(hObject,'String',data);



% --- Executes on slider movement.
function noisedenslider_Callback(hObject, eventdata, handles)
% hObject    handle to noisedenslider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
val=get(handles.noisedenslider,'Value');
set(handles.noisedenval,'String',val);

% --- Executes during object creation, after setting all properties.
function noisedenslider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to noisedenslider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in previewbtn.
function previewbtn_Callback(hObject, eventdata, handles)
% hObject    handle to previewbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global noiseimage;
global noisetype;
global val;
global J;
val=str2double(get(handles.noisedenval,'String'));
set(handles.noisedentxtbox,'String',val);
selnoisetype={get(handles.noisetypelb,'Value')};
%noisetype=str2double(noisetype);
set(handles.noisetypetxtbox,'String',selnoisetype);
noisetype=str2double(get(handles.noisetypetxtbox,'String'));
switch noisetype
    case 1.0
        set(handles.noisetypetxtbox,'String','Gaussian Noise')
        J=imnoise(noiseimage,'gaussian',val);
        axes(handles.axes1);
        imshow(J,[]);
     
    case 2.0
         set(handles.noisetypetxtbox,'String','Poisson Noise')
         J=imnoise(noiseimage,'poisson');
         axes(handles.axes1);
        imshow(J,[]);
    case 3.0
         set(handles.noisetypetxtbox,'String','Salt and Pepper Noise')
          J=imnoise(noiseimage,'salt & pepper',val);
         axes(handles.axes1);
        imshow(J,[]);
    case 4.0
         set(handles.noisetypetxtbox,'String','Speckle Noise')
         J=imnoise(noiseimage,'speckle',val);
         axes(handles.axes1);
        imshow(J,[]);
    otherwise 
        set(handles.noisetypetxtbox,'String','stupid')
end

% --- Executes on button press in setnoisebtn.
function setnoisebtn_Callback(hObject, eventdata, handles)
% hObject    handle to setnoisebtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global J;
global noiseimagepath;
imwrite(J,noiseimagepath,'jpeg');
close();
