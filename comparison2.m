function varargout = comparison2(varargin)
% COMPARISON2 MATLAB code for comparison2.fig
%      COMPARISON2, by itself, creates a new COMPARISON2 or raises the existing
%      singleton*.
%
%      H = COMPARISON2 returns the handle to a new COMPARISON2 or the handle to
%      the existing singleton*.
%
%      COMPARISON2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in COMPARISON2.M with the given input arguments.
%
%      COMPARISON2('Property','Value',...) creates a new COMPARISON2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before comparison2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to comparison2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help comparison2

% Last Modified by GUIDE v2.5 26-Apr-2014 20:49:27

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @comparison2_OpeningFcn, ...
                   'gui_OutputFcn',  @comparison2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before comparison2 is made visible.
function comparison2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to comparison2 (see VARARGIN)

% Choose default command line output for comparison2
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
set(gcf,'Name','Comparisions between Ordered Dithering and Error Diffusion');
global secretimage;
global coverimage;
global secret1;
global cover1;
global secret2;
global cover2;

% UIWAIT makes comparison2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);



% --- Outputs from this function are returned to the command line.
function varargout = comparison2_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes when selected object is changed in uipanel5.
function uipanel5_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanel5 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
global secretimage;
global secret1;
global secret2;
selected=get(eventdata.NewValue,'String');
if strcmp(selected,'Small') == 1
      image=imread('G:\SAKSHI-B67-8SEMPROJECT\IMAGES\secretsmall.jpg');
elseif strcmp(selected,'Medium') == 1
   image=imread('G:\SAKSHI-B67-8SEMPROJECT\IMAGES\secretmed.jpg');
else
    image=imread('G:\SAKSHI-B67-8SEMPROJECT\IMAGES\secretlarge.jpg');
end
secretimage=image;
secret1=image;
secret2=image;
axes(handles.axes1);
imshow(image);


% --- Executes when selected object is changed in uipanel4.
function uipanel4_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanel4 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
global coverimage;
global cover1;
global cover2;
selected=get(eventdata.NewValue,'Tag');
if strcmp(selected,'low') == 1
    image=imread('G:\SAKSHI-B67-8SEMPROJECT\IMAGES\sunset.jpg');
else
    image=imread('G:\SAKSHI-B67-8SEMPROJECT\IMAGES\flowers.jpg');
end
coverimage=image;
cover1=image;
cover2=image;
axes(handles.axes2);
imshow(image);

set(handles.text1,'Visible','on');
set(handles.text11,'Visible','on');
set(handles.text3,'Visible','on');
set(handles.text5,'Visible','on');
set(handles.odnoise,'Visible','on');
set(handles.odden,'Visible','on');
set(handles.odalgo,'Visible','on');

% --- Executes on selection change in odnoise.
function odnoise_Callback(hObject, eventdata, handles)
% hObject    handle to odnoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns odnoise contents as cell array
%        contents{get(hObject,'Value')} returns selected item from odnoise
global noise1;
noise=get(hObject,'Value');
if noise == 1
    noise1='gaussian';
elseif noise == 2
    noise1='poisson'
elseif noise == 3
    noise1 = 'salt & pepper'
else 
    noise1 = 'speckle'
end

% --- Executes during object creation, after setting all properties.
function odnoise_CreateFcn(hObject, eventdata, handles)
% hObject    handle to odnoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in odden.
function odden_Callback(hObject, eventdata, handles)
% hObject    handle to odden (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns odden contents as cell array
%        contents{get(hObject,'Value')} returns selected item from odden
global noiseden1;
noiseden=get(hObject,'Value');
noiseden1=noiseden/10;

% --- Executes during object creation, after setting all properties.
function odden_CreateFcn(hObject, eventdata, handles)
% hObject    handle to odden (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in odalgo.
function odalgo_Callback(hObject, eventdata, handles)
% hObject    handle to odalgo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns odalgo contents as cell array
%        contents{get(hObject,'Value')} returns selected item from odalgo
global val1;
val1=get(hObject,'Value');

set(handles.text9,'Visible','on');
set(handles.text8,'Visible','on');
set(handles.text6,'Visible','on');
set(handles.edalgo,'Visible','on');
set(handles.comparebtn,'Visible','on');

% --- Executes during object creation, after setting all properties.
function odalgo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to odalgo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on selection change in edalgo.
function edalgo_Callback(hObject, eventdata, handles)
% hObject    handle to edalgo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns edalgo contents as cell array
%        contents{get(hObject,'Value')} returns selected item from edalgo
global val2;
val2=get(hObject,'Value');

% --- Executes during object creation, after setting all properties.
function edalgo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edalgo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in comparebtn.
function comparebtn_Callback(hObject, eventdata, handles)
% hObject    handle to comparebtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secret1;
global cover1;
global noise1;
global noiseden1;
global val1;
global onefirstshare;
global onesecshare;
global secret2;
global cover2;
global revealone;
global val2;
global revealtwo;
global twofirstshare;
global twosecshare;

set(handles.uipanel1,'Title','REVEALED IMAGE FROM ORDERED DITHERING ALGORITHM');
set(handles.uipanel2,'Title','REVEALED IMAGE FROM ERROR DIFFUSION ALGORITHM');

secret1=rgb2gray(secret1);
cover1=rgb2gray(cover1);
secret2=rgb2gray(secret2);
cover2=rgb2gray(cover2);

h=size(secret1,1);
w=size(secret1,2);
cover1=imresize(cover1,[h w]);
cover2=imresize(cover2,[h w]);

secret1=imnoise(secret1,noise1,noiseden1);

secret1=im2bw(secret1);
secret2=im2bw(secret2);

if val1 == 1
    S = [34 29 17 21 30 35;
		 28 14 9 16 20 31;
		 13 8 4 5 15 19;
		 12 3 0 1 10 18; 
		 27 7 2 6 23 24;
		 33 26 11 22 25 32];
elseif val1==2
    S = [34 25 21 17 29 33;
		  30 13 9 5 12 24;
		  18 6 1 0 8 20;
		  22 10 2 3 4 16;
		  26 14 7 11 15 28;
		  35 31 19 23 27 32];
elseif val1==3
    S = [30 22 16 21 33 35;
		  24 11 7 9 26 28;
		  13 5 0 2 14 19;
		  15 3 1 4 12 18;
		  27 8 6 10 25 29;
		  32 20 17 23 31 34];
elseif val1==4
    S1 = [13 9 5 12;
		   6 1 0 8;
		   10 2 3 4;
		   14 7 11 15];
   
	S2 = [18 22 26 19;
		   25 30 31 23;
		   21 29 28 27;
		   17 24 20 16];

	S = [S1 S2; S2 S1];
else
    U = ones(3);
	D = [8 4 5;
		  3 0 1;
		  7 2 6];

	S = [4*D 4*D+2*U;
		  4*D+3*U 4*D+U];
end 
si = size(cover1);
ss = size(S);
ts = ceil(si ./ ss);
SImg = repmat(S, ts);
SImg = SImg(1:si(1), 1:si(2));
SImg = SImg + 0.5;
N = max(S(:)) - min(S(:)) + 2;
D = 255 ./ (N-1);
Q = double(cover1) ./ D;
changedimage = Q > SImg;
cover1=changedimage;

h2=im2bw(secret1);
x1=im2bw(cover1);
sizeh=size(h2,1);
sizew=size(h2,2);
x2=zeros(sizeh,sizew);
for i=1:sizeh
    for j=1:sizew
        h2pixel=h2(i,j);
        if h2pixel == 1 %if pixel in h2 is white
            x2(i,j)= x1(i,j);
        else
            x1pixel=x1(i,j);
            comph2pixel=~h2(i,j);
            x2(i,j)=xor(x1pixel,comph2pixel);
        end
    end
end

onefirstshare=cover1;
onesecshare=x2;
revealone=xor(onefirstshare,onesecshare);

axes(handles.axes1);
imshow(revealone);

if val2 == 1
    F = [0 0 7;
		 3 5 1];
	F = 1/sum(F(:)) .* F;
else 
    F = [0 0 0 8 4;
		 2 4 8 4 2;
		 1 2 4 2 1];
	F = 1/sum(F(:)) .* F;
end
I = im2double(cover2);
threshold = 0.5;
sf = size(F);
O = zeros(size(I)); %outputimage
% Zero-padding input image, so that we don't have to deal with edges when
% applying the filter. The extra pixels are removed in the output image
I = padarray(I, sf);
padx = sf(2);
pady = sf(1);
si = size(I);
for y = pady+1:si(1)-pady
    for x = padx+1:si(2)-padx
        
        % Threshold image and save to output (taking into account the zero
        % padding introduced)
        oy = y - pady;
        ox = x - padx;
        O(oy,ox) = (I(y,x) > threshold);
        
        % Calculate error
        error = double(I(y,x) - O(oy,ox));
        
        % Get position where the filter should be applied
        k = floor(sf(2)/2);
        ymin = y;
        ymax = y + sf(1) - 1;
        xmin = x - (sf(2) - k - 1);
        xmax = x + k;
        
        % Distribute error according to the filter
        I(ymin:ymax, xmin:xmax) = I(ymin:ymax, xmin:xmax) + error * F;
    end
end
cover2=O;
    
x3=zeros(h,w);
for i=1:sizeh
    for j=1:sizew
        h2pixel=secret2(i,j);
        if h2pixel == 1 %if pixel in h2 is white
            x3(i,j)= cover2(i,j);
        else
            x1pixel=cover2(i,j);
            comph2pixel=~secret2(i,j);
            x3(i,j)=xor(x1pixel,comph2pixel);
        end
    end
end
secret2=x3;


twofirstshare=cover2;
twosecshare=x3;
revealtwo=xor(secret2,cover2);

axes(handles.axes2);
imshow(revealtwo);

data1={noise1;noiseden1;'have to calculate';'have to calculate';w;h;'Binary'};
set(handles.table1,'Data',data1);
data2={'No noise added';'No noise added. No noise density selected';'have to calculate';'have to calculate';w;h;'Binary';};
set(handles.table2,'Data',data2);

set(handles.backbtn,'Visible','on');
set(handles.text1,'Enable','off');
set(handles.text11,'Enable','off');
set(handles.text3,'Enable','off');
set(handles.text5,'Enable','off');
set(handles.odnoise,'Enable','off');
set(handles.odden,'Enable','off');
set(handles.odalgo,'Enable','off');
set(handles.text9,'Enable','off');
set(handles.text8,'Enable','off');
set(handles.text6,'Enable','off');
set(handles.edalgo,'Enable','off');
set(handles.comparebtn,'Enable','off');
set(handles.text13,'Enable','off');
set(handles.text12,'Enable','off');
set(handles.small,'Enable','off');
set(handles.medium,'Enable','off');
set(handles.large,'Enable','off');
set(handles.low,'Enable','off');
set(handles.high,'Enable','off');



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in secimagetype.
function secimagetype_Callback(hObject, eventdata, handles)
% hObject    handle to secimagetype (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns secimagetype contents as cell array
%        contents{get(hObject,'Value')} returns selected item from secimagetype


% --- Executes during object creation, after setting all properties.
function secimagetype_CreateFcn(hObject, eventdata, handles)
% hObject    handle to secimagetype (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in backbtn.
function backbtn_Callback(hObject, eventdata, handles)
% hObject    handle to backbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
choices();
close(comparison2);

% --- Executes on button press in twofirstbtn.
function twofirstbtn_Callback(hObject, eventdata, handles)
% hObject    handle to twofirstbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global twofirstshare;
setappdata(0,'CompareFullImage',twofirstshare);
fullscreen2();

% --- Executes on button press in twosecbtn.
function twosecbtn_Callback(hObject, eventdata, handles)
% hObject    handle to twosecbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global twosecshare;
setappdata(0,'CompareFullImage',twosecshare);
fullscreen2();


% --- Executes on button press in tworevealbtn.
function tworevealbtn_Callback(hObject, eventdata, handles)
% hObject    handle to tworevealbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global revealtwo;
setappdata(0,'CompareFullImage',revealtwo);
fullscreen2();

% --- Executes on button press in onefirstbtn.
function onefirstbtn_Callback(hObject, eventdata, handles)
% hObject    handle to onefirstbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global onefirstshare;
setappdata(0,'CompareFullImage',onefirstshare);
fullscreen2();

% --- Executes on button press in onesecbtn.
function onesecbtn_Callback(hObject, eventdata, handles)
% hObject    handle to onesecbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global onesecshare;
setappdata(0,'CompareFullImage',onesecshare);
fullscreen2();

% --- Executes on button press in onerevealbtn.
function onerevealbtn_Callback(hObject, eventdata, handles)
% hObject    handle to onerevealbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global revealone;
setappdata(0,'CompareFullImage',revealone);
fullscreen2();
