
function varargout = gui1(varargin)
% GUI1 MATLAB code for gui1.fig
%      GUI1, by itself, creates a new GUI1 or raises the existing
%      singleton*.
%
%      H = GUI1 returns the handle to a new GUI1 or the handle to
%      the existing singleton*.
%
%      GUI1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI1.M with the given input arguments.
%
%      GUI1('Property','Value',...) creates a new GUI1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui1_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui1

% Last Modified by GUIDE v2.5 05-Apr-2014 21:56:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui1_OpeningFcn, ...
                   'gui_OutputFcn',  @gui1_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gui1 is made visible.
function gui1_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui1 (see VARARGIN)

% Choose default command line output for gui1
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gui1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);
set(gcf,'Name','Encrypt Image using Ordered Dithering Algorithm');
setappdata(0,'Step',0);
setappdata(0,'flag',0);%to depict dhcod

% --- Outputs from this function are returned to the command line.
function varargout = gui1_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in uploadsecbutton.
function uploadsecbutton_Callback(hObject, eventdata, handles)
% hObject    handle to uploadsecbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimage;
global secimagepath;
secimagepath=imgetfile;
secimage=imread(secimagepath);
setappdata(0,'SelectedImage',secimagepath);
dialog1();
uiwait(gcf);
secimagepath=getappdata(0,'SelectedImage');
%set(handles.edit9,'String',secimagepath);
secimage=imread(secimagepath);
axes(handles.axes1);
imshow(secimage);
set(handles.secviewprop,'Enable','on');
set(handles.secviewpix,'Enable','on');
set(handles.secimagetable,'Enable','on');
set(handles.uploadcoverbutton,'Enable','on');
set(handles.uploadsecbutton,'Enable','off');
set(handles.secnote,'Visible','on');

% --- Executes on button press in uploadcoverbutton.
function uploadcoverbutton_Callback(hObject, eventdata, handles)
% hObject    handle to uploadcoverbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global covimage;
global covimagepath;
covimagepath=imgetfile;
covimage=imread(covimagepath);
setappdata(0,'SelectedImage',covimagepath);
dialog1();
uiwait(gcf);
covimagepath=getappdata(0,'SelectedImage');
set(handles.edit9,'String',covimagepath);
%covimage=imread(covimagepath);
axes(handles.axes2);
imshow(covimage, []);
set(handles.covviewprop,'Enable','on');
set(handles.covviewpix,'Enable','on');
set(handles.covimagetable,'Enable','on');
set(handles.text3,'Enable','on');
set(handles.sectograybutton,'Enable','on');
set(handles.uploadcoverbutton,'Enable','off');
set(handles.covnote,'Visible','on');
set(handles.secviewprop,'Visible','on');
set(handles.secviewpix,'Visible','on');
set(handles.secimagetable,'Visible','on');
set(handles.covviewprop,'Visible','on');
set(handles.covviewpix,'Visible','on');
set(handles.covimagetable,'Visible','on');
set(handles.text3,'Visible','on');
set(handles.sectograybutton,'Visible','on');
set(handles.covtosecbuton,'Visible','on');
set(handles.uipanel6,'Visible','on');

% --- Executes on button press in sectograybutton.
function sectograybutton_Callback(hObject, eventdata, handles)
% hObject    handle to sectograybutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%jelly=imread('D:\SAKSHI-B67-8SEMPROJECT\Jellyfish.jpg');
global secimage;
global secimagepath;
secimage=rgb2gray(secimage);
imwrite(secimage,secimagepath,'jpeg');
axes(handles.axes1);
imshow(secimage, []);
data={'';'';'';'';'';'';'';'';'';};
set(handles.secimagetable,'Data',data);
set(handles.covtosecbuton,'Enable','on');
set(handles.sectograybutton,'Enable','off');
set(handles.secnote,'String','Image converted to Grayscale');

% --- Executes on button press in covtosecbuton.
function covtosecbuton_Callback(hObject, eventdata, handles)
% hObject    handle to covtosecbuton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%tulips=imread('D:\SAKSHI-B67-8SEMPROJECT\Tulips.jpg');
global covimage;
global covimagepath;
covimage=rgb2gray(covimage);
imwrite(covimage,covimagepath,'jpeg');
axes(handles.axes2);
imshow(covimage, []);
data={'';'';'';'';'';'';'';'';'';};
set(handles.covimagetable,'Data',data);
set(handles.covtosecbuton,'Enable','off');
set(handles.text4,'Enable','on');
set(handles.redresolsecbtn,'Enable','on');
set(handles.covnote,'String','Image converted to Grayscale');
set(handles.text4,'Visible','on');
set(handles.redresolsecbtn,'Visible','on');
set(handles.redresolcovbtn,'Visible','on');

% --- Executes on button press in redresolsecbtn.
function redresolsecbtn_Callback(hObject, eventdata, handles)
% hObject    handle to redresolsecbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimage;
global secimagepath;
setappdata(0,'SharedImage',secimagepath);
reduceresoltool();
uiwait(gcf);
w=getappdata(0,'redresolw');
h=getappdata(0,'redresolh');
secimage=imresize(secimage,[h w]);
axes(handles.axes1);
imshow(secimage,[]);
setappdata(0,'SECRETIMAGE',secimage);
data={'';'';'';'';'';'';'';'';'';};
set(handles.secimagetable,'Data',data);
imwrite(secimage,secimagepath,'jpeg');
set(handles.redresolsecbtn,'Enable','off');
set(handles.redresolcovbtn,'Enable','on');
set(handles.secnote,'String','Image Resized');

% --- Executes on button press in redresolcovbtn.
function redresolcovbtn_Callback(hObject, eventdata, handles)
% hObject    handle to redresolcovbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global covimage;
global covimagepath;
% setappdata(0,'SharedImage',covimagepath);
% reduceresoltool();
% uiwait();
w=getappdata(0,'redresolw');
h=getappdata(0,'redresolh');
% set(handles.edit9,'String',w);
covimage=imresize(covimage,[h w]);
axes(handles.axes2);
imshow(covimage,[]);
setappdata(0,'COVERIMAGE',covimage);
data={'';'';'';'';'';'';'';'';'';};
set(handles.covimagetable,'Data',data);
imwrite(covimage,covimagepath,'jpeg');
set(handles.redresolcovbtn,'Enable','off');
set(handles.text7,'Enable','on');
set(handles.text8,'Enable','on');
set(handles.noisetype,'Enable','on');
set(handles.covnote,'String','Image Scaled');
set(handles.text7,'Visible','on');
set(handles.text8,'Visible','on');
set(handles.text9,'Visible','on');
set(handles.noisetype,'Visible','on');
set(handles.noiseden,'Visible','on');
set(handles.selnoiseden,'Visible','on');
set(handles.previewnoise,'Visible','on');


% --- Executes during object creation, after setting all properties.
function secimagetable_CreateFcn(hObject, eventdata, handles)
% hObject    handle to secimagetable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in secviewprop.
function secviewprop_Callback(hObject, eventdata, handles)
% hObject    handle to secviewprop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimage;
global secimagepath;
info=imfinfo(secimagepath);
comprat=(((info.Width*info.Height*info.BitDepth)/8)/info.FileSize);
step=getappdata(0,'Step');
if step == 5
    data={info.Filename;info.FileModDate;info.FileSize;info.Format;info.Width;info.Height;info.BitDepth;comprat;'Grayscale-Binary';};
else
    data={info.Filename;info.FileModDate;info.FileSize;info.Format;info.Width;info.Height;info.BitDepth;comprat;info.ColorType;};
end
set(handles.secimagetable,'Data',data);

% --- Executes on button press in secviewpix.
function secviewpix_Callback(hObject, eventdata, handles)
% hObject    handle to secviewpix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%jelly=imread('D:\SAKSHI-B67-8SEMPROJECT\Jellyfish.jpg');
global secimage;
global secimagepath;
setappdata(0,'SharedImage',secimagepath);
pixelregiontool();

% --- Executes on button press in covviewprop.
function covviewprop_Callback(~, eventdata, handles)
% hObject    handle to covviewprop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global covimage;
global covimagepath;
info=imfinfo(covimagepath);
comprat=(((info.Width*info.Height*info.BitDepth)/8)/info.FileSize);
step=getappdata(0,'Step');
if step == 5
    data={info.Filename;info.FileModDate;info.FileSize;info.Format;info.Width;info.Height;info.BitDepth;comprat;'Grayscale-Binary';};
else
    data={info.Filename;info.FileModDate;info.FileSize;info.Format;info.Width;info.Height;info.BitDepth;comprat;info.ColorType;};
end
set(handles.covimagetable,'Data',data);

% --- Executes on button press in covviewpix.
function covviewpix_Callback(hObject, eventdata, handles)
% hObject    handle to covviewpix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global covimage;
global covimagepath;
setappdata(0,'SharedImage',covimagepath);
pixelregiontool();

% --- Executes on button press in htcovbtn.
function htcovbtn_Callback(hObject, eventdata, handles)
% hObject    handle to htcovbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global covimage;
global covimagepath;
setappdata(0,'SharedImage',covimagepath);
halftonebydithering();
uiwait();
data={'';'';'';'';'';'';'';'';'';};
set(handles.covimagetable,'Data',data);
covimagepath=getappdata(0,'SharedImage');
covimage=imread(covimagepath);
axes(handles.axes2);
imshow(covimage);
setappdata(0,'firstshare',covimage);
set(handles.note,'Visible','on');
set(handles.covnote,'String','Dithered Halftone Image Generated');
set(handles.text22,'Enable','on');
set(handles.gensecsharebtn,'Enable','on');
set(handles.htcovbtn,'Enable','off');
set(handles.text22,'Visible','on');
set(handles.gensecsharebtn,'Visible','on');


% --- Executes on button press in calthresbtn.
function calthresbtn_Callback(hObject, eventdata, handles)
% hObject    handle to calthresbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimage;
global secimagepath; 
global val;
val=graythresh(secimage);
set(handles.thresvaltxt,'String',val);
set(handles.htsecbtn,'Enable','on');
set(handles.calthresbtn,'Enable','off');
set(handles.thresvaltxt,'Enable','inactive');

% --- Executes on button press in htsecbtn.
function htsecbtn_Callback(hObject, eventdata, handles)
% hObject    handle to htsecbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimage;
global secimagepath;
global val;
setappdata(0,'Step',5);
secimagetry=im2bw(secimage,val);
axes(handles.axes1);
imshow(secimagetry,[]);
imwrite(secimagetry,secimagepath,'jpeg');
data={'';'';'';'';'';'';'';'';'';};
set(handles.secimagetable,'Data',data);
set(handles.htsecbtn,'Enable','off');
set(handles.text12,'Enable','on');
set(handles.thresvaltxt,'Enable','off');
set(handles.htcovbtn,'Enable','on');
set(handles.secnote,'String','Image Converted to Binary');
set(handles.text12,'Visible','on');
set(handles.htcovbtn,'Visible','on');

% --- Executes on button press in noisesecbtn.
function noisesecbtn_Callback(hObject, eventdata, handles)
% hObject    handle to noisesecbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in noisetype.
function noisetype_Callback(hObject, eventdata, handles)
% hObject    handle to noisetype (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns noisetype contents as cell array
%        contents{get(hObject,'Value')} returns selected item from noisetype
i=get(hObject,'Value');
if i==1
    set(handles.noiseden,'Enable','on');
    set(handles.selnoiseden,'Enable','on');
    set(handles.text9,'Enable','on');
elseif i==2
   set(handles.noiseden,'Enable','off');
   set(handles.selnoiseden,'Enable','off');
   set(handles.text9,'Enable','off');
elseif i==3
    set(handles.noiseden,'Enable','on');
    set(handles.selnoiseden,'Enable','on');
    set(handles.text9,'Enable','on');
else
     set(handles.noiseden,'Enable','on');
    set(handles.selnoiseden,'Enable','on');
    set(handles.text9,'Enable','on');
end
set(handles.text9,'Enable','on');
set(handles.noiseden,'Enable','on');
set(handles.selnoiseden,'Enable','on');


% --- Executes during object creation, after setting all properties.
function noisetype_CreateFcn(hObject, eventdata, handles)
% hObject    handle to noisetype (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
data={'Gaussian noise';'Poisson Noise';'Salt and Pepper noise';'Speckle';};
set(hObject,'String',data);
%set(hObject,data);


% --- Executes on slider movement.
function noiseden_Callback(hObject, eventdata, handles)
% hObject    handle to noiseden (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
val=get(handles.noiseden,'Value');
set(handles.selnoiseden,'String',val);
set(handles.previewnoise,'Enable','on');

% --- Executes during object creation, after setting all properties.
function noiseden_CreateFcn(hObject, eventdata, handles)
% hObject    handle to noiseden (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in previewnoise.
function previewnoise_Callback(hObject, eventdata, handles)
% hObject    handle to previewnoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%contents = (get(handles.noisetype,'String'));
global secimage;
global secimagepath;
setappdata(0,'SharedImage',secimagepath);
contents= {get(handles.noisetype,'Value')} ;%gets 1,2,3,4
set(handles.edit9,'String',contents);
getcontents=str2double(get(handles.edit9,'String'));
val=str2double(get(handles.selnoiseden,'String'));
setappdata(0,'NoiseType',getcontents);
setappdata(0,'NoiseDen',val);
previewnoisetool();
uiwait();
secimage=imread(secimagepath);
axes(handles.axes1);
imshow(secimage,[]);
set(handles.text8,'Enable','off');
set(handles.text9,'Enable','off');
set(handles.noisetype,'Enable','off');
set(handles.noiseden,'Enable','off');
set(handles.selnoiseden,'Enable','off');
set(handles.previewnoise,'Enable','off');
set(handles.text11,'Enable','on');
set(handles.secnote,'String','Noise added to Image');
set(handles.calthresbtn,'Enable','on');
set(handles.thresvaltxt,'Enable','on');
set(handles.text11,'Visible','on');
set(handles.calthresbtn,'Visible','on');
set(handles.thresvaltxt,'Visible','on');
set(handles.htsecbtn,'Visible','on');

function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in gensecsharebtn.
function gensecsharebtn_Callback(hObject, eventdata, handles)
% hObject    handle to gensecsharebtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimagepath;
global secimage;
global covimagepath;
global covimage;

secimage=imread(secimagepath);
h2=im2bw(secimage);
%compsimage=~simage;

covimage=imread(covimagepath);
x1=im2bw(covimage);

sizeh=size(h2,1);
sizew=size(h2,2);

x2=zeros(sizeh,sizew);
%xoredimage=xor(cimage,compsimage);

for i=1:sizeh
    for j=1:sizew
        h2pixel=h2(i,j);
        if h2pixel == 1 %if pixel in h2 is white
            x2(i,j)= x1(i,j);
        else
            x1pixel=x1(i,j);
            comph2pixel=~h2(i,j);
            x2(i,j)=xor(x1pixel,comph2pixel);
        end
    end
end

%resultingimage=image(matriximage);
axes(handles.axes1);
imshow(x2);
imwrite(x2,secimagepath,'jpeg');
setappdata(0,'secondshare',x2);
set(handles.text24,'Visible','on');
set(handles.nextbtn,'Visible','on');
set(handles.nextbtn,'Enable','on');

function edit0_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in nextbtn.
function nextbtn_Callback(hObject, eventdata, handles)
% hObject    handle to nextbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global secimagepath;
global covimagepath;
setappdata(0,'firstpath',covimagepath);
setappdata(0,'secpath',secimagepath);
decryptmain();
close(gui1);


% --- Executes on button press in help1.
function help1_Callback(hObject, eventdata, handles)
% hObject    handle to help1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in help2.
function help2_Callback(hObject, eventdata, handles)
% hObject    handle to help2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in help3.
function help3_Callback(hObject, eventdata, handles)
% hObject    handle to help3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in help7.
function help7_Callback(hObject, eventdata, handles)
% hObject    handle to help7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in help5.
function help5_Callback(hObject, eventdata, handles)
% hObject    handle to help5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in help6.
function help6_Callback(hObject, eventdata, handles)
% hObject    handle to help6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in help4.
function help4_Callback(hObject, eventdata, handles)
% hObject    handle to help4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
