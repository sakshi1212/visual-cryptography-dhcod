function varargout = halftonebydithering(varargin)
% HALFTONEBYDITHERING MATLAB code for halftonebydithering.fig
%      HALFTONEBYDITHERING, by itself, creates a new HALFTONEBYDITHERING or raises the existing
%      singleton*.
%
%      H = HALFTONEBYDITHERING returns the handle to a new HALFTONEBYDITHERING or the handle to
%      the existing singleton*.
%
%      HALFTONEBYDITHERING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in HALFTONEBYDITHERING.M with the given input arguments.
%
%      HALFTONEBYDITHERING('Property','Value',...) creates a new HALFTONEBYDITHERING or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before halftonebydithering_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to halftonebydithering_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help halftonebydithering

% Last Modified by GUIDE v2.5 19-Mar-2014 20:35:35

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @halftonebydithering_OpeningFcn, ...
                   'gui_OutputFcn',  @halftonebydithering_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before halftonebydithering is made visible.
function halftonebydithering_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to halftonebydithering (see VARARGIN)

% Choose default command line output for halftonebydithering
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
set(gcf,'Name','Convert To halftone');
global halftoneimagepath;
global halftoneimage;
halftoneimagepath=getappdata(0,'SharedImage');
halftoneimage=imread(halftoneimagepath);
axes(handles.axes1);
imshow(halftoneimage,[]);
% UIWAIT makes halftonebydithering wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = halftonebydithering_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in previewbtn.
function previewbtn_Callback(hObject, eventdata, handles)
% hObject    handle to previewbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global halftoneimage;
global changedimage;
val1=get(handles.one,'Value');
val2=get(handles.two,'Value');
val3=get(handles.three,'Value');
val4=get(handles.four,'Value');
val5=get(handles.five,'Value');

if val1 == 1
    S = [34 29 17 21 30 35;
		 28 14 9 16 20 31;
		 13 8 4 5 15 19;
		 12 3 0 1 10 18; 
		 27 7 2 6 23 24;
		 33 26 11 22 25 32];
    
elseif val2==1
    S = [34 25 21 17 29 33;
		  30 13 9 5 12 24;
		  18 6 1 0 8 20;
		  22 10 2 3 4 16;
		  26 14 7 11 15 28;
		  35 31 19 23 27 32];
    
elseif val3==1
    S = [30 22 16 21 33 35;
		  24 11 7 9 26 28;
		  13 5 0 2 14 19;
		  15 3 1 4 12 18;
		  27 8 6 10 25 29;
		  32 20 17 23 31 34];
      
elseif val4==1
    S1 = [13 9 5 12;
		   6 1 0 8;
		   10 2 3 4;
		   14 7 11 15];
   
	S2 = [18 22 26 19;
		   25 30 31 23;
		   21 29 28 27;
		   17 24 20 16];

	S = [S1 S2; S2 S1];
   
else
    U = ones(3);
	D = [8 4 5;
		  3 0 1;
		  7 2 6];

	S = [4*D 4*D+2*U;
		  4*D+3*U 4*D+U];

end 

si = size(halftoneimage);
ss = size(S);
ts = ceil(si ./ ss);
SImg = repmat(S, ts);
SImg = SImg(1:si(1), 1:si(2));
SImg = SImg + 0.5;
N = max(S(:)) - min(S(:)) + 2;
D = 255 ./ (N-1);
Q = double(halftoneimage) ./ D;
changedimage = Q > SImg;
axes(handles.axes1);
imshow(changedimage,[]);
        
        

% --- Executes on button press in setimagebtn.
function setimagebtn_Callback(hObject, eventdata, handles)
% hObject    handle to setimagebtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global halftoneimagepath;
global changedimage;
imwrite(changedimage,halftoneimagepath,'jpeg');
close();

% --- Executes during object creation, after setting all properties.
function methodbtngrp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to methodbtngrp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
