function varargout = choices(varargin)
% CHOICES MATLAB code for choices.fig
%      CHOICES, by itself, creates a new CHOICES or raises the existing
%      singleton*.
%
%      H = CHOICES returns the handle to a new CHOICES or the handle to
%      the existing singleton*.
%
%      CHOICES('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CHOICES.M with the given input arguments.
%
%      CHOICES('Property','Value',...) creates a new CHOICES or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before choices_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to choices_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help choices

% Last Modified by GUIDE v2.5 30-Apr-2014 02:26:05

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @choices_OpeningFcn, ...
                   'gui_OutputFcn',  @choices_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before choices is made visible.
function choices_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to choices (see VARARGIN)

% Choose default command line output for choices
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
set(gcf,'Name','Choices');
flag=getappdata(0,'flag');
if flag == 1
    set(handles.dhcedbtn,'Enable','off');
end
% UIWAIT makes choices wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = choices_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in dhcedbtn.
function dhcedbtn_Callback(hObject, eventdata, handles)
% hObject    handle to dhcedbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
dhcedtool();
close(choices);

% --- Executes on button press in dhcodbtn.
function dhcodbtn_Callback(hObject, eventdata, handles)
% hObject    handle to dhcodbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
encryptmain();
close(choices);

% --- Executes on button press in comp1btn.
function comp1btn_Callback(hObject, eventdata, handles)
% hObject    handle to comp1btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
odcomp()
close(choices);

% --- Executes on button press in comp2btn.
function comp2btn_Callback(hObject, eventdata, handles)
% hObject    handle to comp2btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
comparison2();
close(choices);

% --- Executes on button press in closebtn.
function closebtn_Callback(hObject, eventdata, handles)
% hObject    handle to closebtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(choices);


% --- Executes on button press in improvementbtn.
function improvementbtn_Callback(hObject, eventdata, handles)
% hObject    handle to improvementbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
improve();
close(choices);
