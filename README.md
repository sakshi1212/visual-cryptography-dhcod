•	**Visual Cryptography - Data Hiding in Halftone Images using Conjugate Ordered Dithering (DHCOD) Algorithm**  

-	Using Image Processing Toolbox in **MATLAB**

-	An image is encrypted into multiple shares and then watermarked with a cover image to ensure secure image sharing. Comparisons of different types of ordered dithering, insertion of different noise types, and comparison to error diffusion algorithm.

-       Final Semester Project of Undergraduate Degree

Further Documentation : https://drive.google.com/open?id=0B5BkttoARfGUb0FuWEluX1prWnM
Presentation : https://drive.google.com/open?id=0B5BkttoARfGUdEtwZG1mY0lKZ0E